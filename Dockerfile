FROM logstash:5.5.2-alpine


RUN logstash-plugin install logstash-filter-kubernetes_metadata logstash-filter-kubernetes logstash-filter-multiline logstash-output-elasticsearch

CMD ["-f", "/etc/logstash/logstash.conf"]